import abstractScapholdProvider from './abstractScapholdProvider';

export default class Token extends abstractScapholdProvider {
  a = 'a';
  getMessage() {
    window.console.log(`Get messages called ${this.a}`);
  }
}
