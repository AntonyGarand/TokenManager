import ApolloClient, { HttpLink } from 'apollo-client-preset';
import gql from 'graphql-tag';
// import { graphql } from 'graphql';

export default class abstractScapholdProvider {
  static scapholdUrl = 'us-west-2.api.scaphold.io/graphql/token-dev';
  static graphqlUrl = `https://${abstractScapholdProvider.scapholdUrl}`;
  static username = 'user';
  static password = 'user';

  static client = new ApolloClient({
    link: new HttpLink({
      uri: abstractScapholdProvider.graphqlUrl,
    }),
    initialState: {},
  });

  constructor() {
    const LoginUserMutation = gql `
      mutation LoginUserMutation($data: LoginUserInput!) {
        loginUser(input: $data) {
          token
        }
      }
    `;
    window.console.log('abstract constructor');
    abstractScapholdProvider.client.mutate(
      {
        query: LoginUserMutation,
        variables: {
          username: 'user',
          password: 'user',
        },
      })
      .then(window.console.log)
      .catch(window.console.error);
  }
}
