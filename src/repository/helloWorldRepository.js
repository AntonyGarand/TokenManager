import gql from 'graphql-tag';
import makeRepo from './abstractRepository';

const db = makeRepo();
export default function getLatestMessage() {
  return db.query({
    query: gql`
        query GetAllUsers {
          viewer {
            allHellos {
              edges {
                node {
                  id
                  message
                }
              }
            }
          }
        }
      `,
  });
}
