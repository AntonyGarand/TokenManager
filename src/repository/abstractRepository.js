import ApolloClient, { HttpLink } from 'apollo-client-preset';

export default function makeApolloClient() {
  const scapholdUrl = 'us-west-2.api.scaphold.io/graphql/token-dev';
  const graphqlUrl = `https://${scapholdUrl}`;
  // const websocketUrl = `wss://${scapholdUrl}`;


  const graphClient = new ApolloClient({
    link: new HttpLink({
      uri: graphqlUrl,
    }),
    initialState: {},
  });
  return graphClient;
}
